
import Favoris from './components/Favoris'
import './App.css'
import BestMenus from './components/BestMenus'
import menuHotCaketerias from './assets/menu-hot-caketerias.jpeg'
import menuDesayunos from './assets/menu-desayunos.jpeg'
import menuOtros from './assets/menu-otros-desayunos.jpeg'
import './components/Favoris.css'
import './components/PastilleVerte.css'
import Pizza from './assets/category-pizza.png'
import Sushi from './assets/category-sushi.png'
import Hamburger from './assets/category-hamburger.png'
import Veggie from './assets/category-veggie.png'
import Soupe from './assets/category-soup.png'
import Dessert from './assets/category-dessert.png'
import Fondas from './components/Fondas.jsx';
import fondasDonaLaura from './assets/dish-dona-laura.jpeg'
import fondasCottidiene from './assets/dish-cottidiene.jpeg'
import fondasQuerreque from './assets/dish-querreque.jpeg'
import fondasRosaCafe from './assets/dish-rosa-cafe.jpeg'
import Header from './components/Header.jsx'
import Hero from './assets/header-hero.png'
import './components/Header.css'
import './components/Navigation.css'
import Title from './components/Title.jsx'


function App() {

  return (
    <>
      <Header
        title="La comida que ya conoces al mejor precio"
        caption="De tu fonda favorita"
        hero={Hero} />

      <main>

        <Title title="Las favoritas" subtitle="" categories="CATEGORIES" />

        <section className='section-favoris'>
          <Favoris
            icon={Pizza}
            category="Pizza"
            price="À partir de 20€"
            number="42"
            link=" " />

          <Favoris
            icon={Sushi}
            category="Sushi"
            price="À partir de 30€"
            number="35"
            link=" " />

          <Favoris
            icon={Hamburger}
            category="Hamburger"
            price="À partir de 20€"
            number="28"
            link=" " />

          <Favoris
            icon={Veggie}
            category="Veggie"
            price="À partir de 30€"
            number="23"
            link=" " />

          <Favoris
            icon={Soupe}
            category="Soupes"
            price="À partir de 50€"
            number="15"
            link=" " />

          <Favoris
            icon={Dessert}
            category="Dessert"
            price="À partir de 20€"
            number="9"
            link=" " />

        </section>

        <Title title="Fondas cercanas" subtitle="Estás son las fondas que se encuentran cerca!" categories="AUBERGES" />


        <Fondas
          note="9.8"
          image={fondasDonaLaura}
          alt="Dona Laura"
          time="20-30 min"
          category="Fonditas"
          title="Dona Laura"
        />
        <Fondas
          note="7.3"
          image={fondasCottidiene}
          alt="Cottidiene"
          time="20-30 min"
          category="Fonditas"
          title="Cottidiene"
        />
        <Fondas
          note="8.0"
          image={fondasQuerreque}
          alt="Querreque"
          time="20-30 min"
          category="Fonditas"
          title="Querreque"
        />
        <Fondas
          note="8.5"
          image={fondasRosaCafe}
          alt="Rosa Cafe"
          time="20-30 min"
          category="Fonditas"
          title="Rosa Cafe"
        />


        <Title title="Los mejoras menus" subtitle="Aquí están los mejores menús de la semana, y decide que vas a pedir" categories="MENUS" />


        <BestMenus
          img={menuHotCaketerias}
          alt="hot caketerias"
          tag="Hot cakes"
          title="Hot caketerias"
          description="Incluye dos toppics"
          time="20-30 min"
          price="70"
          rate="9.8"
        />

        <BestMenus
          img={menuDesayunos}
          alt="Desayunos"
          tag="Continental"
          title="Desayunos"
          description="Incluye huevo y tostadas"
          time="15-20 min"
          price="50"
          rate="9.8"
        />

        <BestMenus
          img={menuOtros}
          alt="Otros desayunos"
          tag="Hot cakes"
          title="Otros desayunos"
          description="Incluye dos jugos y tocinito"
          time="~20 min"
          price="50"
          rate="9.8"
        />
      </main>
    </>
  )
}

export default App
