import './BestMenus.css'
import Rating from './Rating.jsx'

function BestMenus(props) {

    const { img, alt, tag, title, description, time, price, rate } = props

    return (
        <div className='BestMenus_card'>
            <div className='BestMenus_image-container'>
                <Rating note={rate} />
                <img src={img} alt={alt} />
            </div>

            <div className='BestMenus_info'>
                <h3> {tag}</h3>
                <h2> {title}</h2>
                <p className='BestMenus_light-text'> {description}</p>
                <div className='BestMenus_sub-info'>
                    <p>{time}</p>
                    <p className='BestMenus_light-text'> Desde ${price}</p>
                </div>
            </div>

        </div>
    )

}

export default BestMenus