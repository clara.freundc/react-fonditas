function Favoris (props) {

    return (
        <>
        <section className="favoris-card">
            <a href={props.link}>
                <div className="favoris-bubble">
                    {props.number}
                </div>
                <div className="favoris-round">
                    <img src={props.icon} alt={props.category}></img>
                </div>
                <div className="favoris-infos">
                    <h3>{props.category}</h3>
                    <p>{props.price}</p>
                </div>
            </a>
        </section>
        </>
    )
}


export default Favoris