import "./Fondas.css";
import Rating from "./Rating";

function Fondas(props) {
    return (
        <div className="card-fondas">

            <img className="fondas-image" src={props.image} alt={props.alt} />
            <Rating className="fondas-rating" note={props.note} />
            <div className="fondas-info">
                <div className="title-fondas">
                    {props.title}
                </div>
                <div className="fondas-sub-info">
                    <div className="fondas-time" >
                        {props.time}
                    </div>
                    <div className="fondas-category">
                        {props.category}
                    </div>
                </div>
            </div>

        </div>
    )

}

export default Fondas;