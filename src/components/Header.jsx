import Navigation from "./Navigation"

function Header (props) {

    return (
        <>
        <section className="header-hero">
            <div className="header-header">
                <h1>Fonditas</h1>
                <Navigation
                nav1 = "Nosotros"
                link1 = " "
                nav2 = "Fonditas"
                link2 = " "
                nav3 = "Mapa"
                link3 = " "
                nav4 = "Inscribirse"
                link4 = " " />   
            </div>
            <div className="header-content">
                <div className="header-infos">
                    <h2>{props.caption}</h2>
                    <h1>{props.title}</h1>
                </div>
                <img src={props.hero} alt=""></img>
            </div>
        </section>
        </>
    )
}


export default Header