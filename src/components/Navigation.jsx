function Navigation (props) {

    return (
        <>
        <div className="navigation-bloc">
            <nav className="navigation-nav">
                <ul>
                    <li><a href={props.link1}>{props.nav1}</a></li>
                    <li><a href={props.link2}>{props.nav2}</a></li>
                    <li><a href={props.link3}>{props.nav3}</a></li>
                    <li><a href={props.link4}>{props.nav4}</a></li>
                </ul>
            </nav>
        </div>
        </>
    )
}


export default Navigation