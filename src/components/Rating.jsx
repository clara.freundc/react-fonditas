import "./Rating.css";
import star from "../assets/star.png";


function Rating(props){

    return (
        <div className="rating-object">
            <img className="star-icon" src={star} />
            <div className="note">{props.note}</div>
        </div>
    );
}

export default Rating;