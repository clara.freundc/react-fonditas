import './Title.css'
import './PastilleVerte.jsx'
import PastilleVerte from './PastilleVerte.jsx';

function Title(props) {

    const { title, subtitle, categories } = props;

    return (
        <div className="Title_component">
            <PastilleVerte categories={categories}/>
            <h2 className="Title_component--title"> {title} </h2>
            <p className="Title_component--subtitle"> {subtitle} </p>
        </div>
    )
}

export default Title